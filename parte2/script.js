var movingCursor = false;
let colorInput = document.getElementById('color');

function colorRnd() {
    var bubbles = document.querySelectorAll('.circle-inner');
    bubbles.forEach(element => {
        var randomColor = Math.floor(Math.random()*16777215).toString(16);
        element.style.border = "solid 1px #" + randomColor;
        element.style.boxShadow = "0 0.5vw 0.75vw #0001, inset 0 0.25vw 0.75vw 0.125vw #"+ randomColor;
    });
}

function colorInicial() {
    var bubbles = document.querySelectorAll('.circle-inner');
    bubbles.forEach(element => {
        element.style.border = "solid 1px white";
        element.style.boxShadow = "0 0.5vw 0.75vw #0001, inset 0 0.25vw 0.75vw 0.125vw white";
    });
}

function deleteRandom() {
    var bubbles = document.getElementById("elements");
    if (bubbles.children.length > 0) {
        var i = Math.random() * bubbles.children.length;
        var b = bubbles.children.item(i)
        deleteBubble(b);
    }
}

function deleteBubble(b) {
    var bubbles = document.getElementById("elements");
    b.style.transition = 'all 0.3s ease-out';
    b.style.width = '0';
    b.style.height = '0';
    setTimeout(() => bubbles.removeChild(b), 300);
}

function newBubble() {
    for (let i = Math.random() * 5 + 1; i > 0; i--) {
        var circle = document.createElement("div");
        circle.classList.add("circle");
        var circleinner = document.createElement("div");
        circleinner.classList.add("circle-inner")
        circle.appendChild(circleinner);
        document.getElementById("elements").appendChild(circle);
        var size = Math.random() * 15 + 1;
        circle.style.width = size + "vw";
        circle.style.height = size + "vw";
        circleinner.style.border = "solid 1px " + colorInput.value;
        circleinner.style.boxShadow = "0 0.5vw 0.75vw #0001, inset 0 0.25vw 0.75vw 0.125vw " + colorInput.value;
        circle.setAttribute('onclick', "deleteBubble(this)");
    }
}

function selectElements(move) {
    movingCursor = move;
    var cursor = document.querySelectorAll('.circle');
    cursor.forEach(element => {
        if (movingCursor) {
            followCursor(element);
        }
        else {
            detenerFollow(element);
        }
    });
}

function detenerFollow(cursor) {
    cursor.style.pointerEvents = 'unset';
    cursor.style.position = 'unset';
    cursor.style.left = 'unset';
    cursor.style.top = 'unset';
    cursor.style.transform = 'translate3d(0, 0, 0)';
}

function followCursor(cursor) {
    cursor.style.pointerEvents = 'none';
    cursor.style.position = 'fixed';
    cursor.style.top = '0';
    cursor.style.left = '0';
    cursor.style.transform = 'translate3d(-50%, -50%, 0)';
    let mouse = { x: 0, y: 0 };
    let pos = { x: 0, y: 0 };
    const speed = Math.random(); // between 0 and 1
    const updatePosition = () => {
        pos.x += (mouse.x - pos.x) * speed;
        pos.y += (mouse.y - pos.y) * speed;
        cursor.style.transform = 'translate3d(' + pos.x + 'px ,' + pos.y + 'px, 0)';
    };
    var count = 0;
    var xdif = Math.random() * 500 - 250;
    var ydif = Math.random() * 500 - 250;
    const updateCoordinates = e => {
        if (count > 70) {
            xdif = Math.random() * 600 - 300;
            ydif = Math.random() * 600 - 300;
            count = 0;
        }
        count++;
        mouse.x = e.clientX + xdif;
        mouse.y = e.clientY + ydif;
    }
    window.addEventListener('mousemove', updateCoordinates);
    function loop() {
        if (movingCursor) {
            updatePosition();
            requestAnimationFrame(loop);
        }
    }
    if (movingCursor)
        requestAnimationFrame(loop);
}